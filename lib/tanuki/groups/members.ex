defmodule Tanuki.Groups.Members do
  @doc """
  GET /groups/:id/members

  Get a list of group members viewable by the authenticated user.
  """
  def list(id, client), do: Tanuki.get("groups/#{id}/members", client)

  @doc """
  POST /groups/:id/members

  Adds a user to the list of group members.

  Parameters:
  - user_id (required) - The ID of a user to add
  - access_level (required) - Project access level
  """
  def create(id, client, params), do: Tanuki.post("groups/#{id}/members", client, params)

  @doc """
  PUT /groups/:id/members/:user_id

  Updates a group team member to a specified access level.

  Parameters:
  - access_level (required) - Project access level
  """
  def modify(id, user_id, client, params), do: Tanuki.put("groups/#{id}/members/#{user_id}", client, params)

  @doc """
  DELETE /groups/:id/members/:user_id

  Removes user from user team.
  """
  def delete(id, user_id, client), do: Tanuki.delete("groups/#{id}/members/#{user_id}", client)
end
