defmodule Tanuki.Settings do
  @doc """
  GET /application/settings

  Get current application settings
  """
  def list(client), do: Tanuki.get("application/settings", client)

  @doc """
  PUT /application/settings

  Change application settings

  Parameters:
  - default_projects_limit - project limit per user
  - signup_enabled - enable registration
  - signin_enabled - enable login via GitLab account
  - gravatar_enabled - enable gravatar
  - sign_in_text - text on login page
  - home_page_url - redirect to this URL when not logged in
  - default_branch_protection - determine if developers can push to master
  - twitter_sharing_enabled - allow users to share project creation in twitter
  - restricted_visibility_levels - restrict certain visibility levels
  - max_attachment_size - limit attachment size
  - session_expire_delay - session lifetime
  - default_project_visibility - what visibility level new project receives
  - default_snippet_visibility - what visibility level new snippet receives
  - restricted_signup_domains - force people to use only corporate emails for signup
  - user_oauth_applications - allow users to create oauth applications
  - after_sign_out_path - where redirect user after logout
  """
  def modify(client, params), do: Tanuki.put("application/settings", client, params)
end
