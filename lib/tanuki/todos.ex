defmodule Tanuki.Todos do
  @doc """
  Returns a list of todos. When no filter is applied, it returns all pending
  todos for the current user. Different filters allow the user to precise the
  request.
  """
  def list(client, params \\ []), do: Tanuki.get("todos", client, params)

  @doc """
  Gets the user by id, response might vary on your own role
  """
  def done(todo_id, client), do: Tanuki.delete("todos/#{todo_id}", client)

  @doc """
  Mark all your todo's as done.
  """
  def all_done(client), do: Tanuki.delete("todos", client)
end
