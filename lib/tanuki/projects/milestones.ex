defmodule Tanuki.Projects.Milestones do
  @doc """
  GET /projects/:id/milestones

  Returns a list of project milestones.

  Parameters:
  iid(optional) - Return the milestone having the given iid
  """
  def list(id, client, params \\ []), do: Tanuki.get("projects/#{id}/milestones", client, params)

  @doc """
  GET /projects/:id/milestones/:milestone_id

  Gets a single project milestone.
  """
  def find(id, milestone_id, client), do: Tanuki.get("projects/#{id}/milestones/#{milestone_id}", client)

  @doc """
  POST /projects/:id/milestones

  Creates a new project milestone.

  Parameters:
  - title (required) - The title of an milestone
  - description (optional) - The description of the milestone
  - due_date (optional) - The due date of the milestone
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/milestones", client, params)

  @doc """
  PUT /projects/:id/milestones/:milestone_id

  Updates an existing project milestone.

  Parameters:
  - title (optional) - The title of a milestone
  - description (optional) - The description of a milestone
  - due_date (optional) - The due date of the milestone
  - state_event (optional) - The state event of the milestone (close|activate)
  """
  def modify(id, milestone_id, client, params), do: Tanuki.put("projects/#{id}/milestones/#{milestone_id}", client, params)

  @doc """
  GET /projects/:id/milestones/:milestone_id/issues

  Gets all issues assigned to a single project milestone.
  """
  def issues(id, milestone_id, client), do: Tanuki.get("projects/#{id}/milestones/#{milestone_id}/issues", client)
end
