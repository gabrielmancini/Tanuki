defmodule Tanuki.Projects.Uploads do
  @moduledoc """
  Used for uploading files for later reference in issues / merge requests
  """

  @doc """
  POST /projects/:id/uploads

  Get a list of project snippets.
  """
  def create(id, client), do: Tanuki.post("projects/#{id}/uploads", client)
end
