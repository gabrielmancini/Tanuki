defmodule Tanuki.Projects.MergeRequests do
  @doc """
  GET /projects/:id/merge_requests

  Get all merge requests for this project. The state parameter can be used to get only merge requests with a given state (opened, closed, or merged) or all of them (all). The pagination parameters page and per_page can be used to restrict the list of merge requests.

  Parameters:
  - iid (optional) - Return the request having the given iid
  - state (optional) - Return all requests or just those that are merged, opened or closed
  - order_by (optional) - Return requests ordered by created_at or updated_at fields. Default is created_at
  - sort (optional) - Return requests sorted in asc or desc order. Default is desc
  """
  def list(id, client, params \\ []), do: Tanuki.get("projects/#{id}/merge_requests", client, params)

  @doc """
  GET /projects/:id/merge_request/:merge_request_id

  Lists a specific Merge Request of a project.
  """
  def find(id, mr_id, client), do: Tanuki.get("projects/#{id}/merge_request/#{mr_id}", client)

  @doc """
  GET /projects/:id/merge_request/:merge_request_id/commits

  Get a list of merge request commits.
  """
  def commits(id, mr_id, client), do: Tanuki.get("projects/#{id}/merge_request/#{mr_id}/commits", client)

  @doc """
  GET /projects/:id/merge_request/:merge_request_id/changes

  Shows information about the merge request including its files and changes.
  """
  def changes(id, mr_id, client), do: Tanuki.get("projects/#{id}/merge_request/#{mr_id}/changes", client)

  @doc """
  POST /projects/:id/merge_requests

  Creates a new merge request.

  Parameters:
  -  source_branch (required) - The source branch
  -  target_branch (required) - The target branch
  -  assignee_id (optional) - Assignee user ID
  -  title (required) - Title of MR
  -  description (optional) - Description of MR
  -  target_project_id (optional) - The target project (numeric id)
  -  labels (optional) - Labels for MR as a comma-separated list
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/merge_requests", client, params)

  @doc """
  PUT /projects/:id/merge_request/:merge_request_id

  Updates an existing merge request. You can change the target branch, title, or even close the MR.

  Parameters:
  - target_branch - The target branch
  - assignee_id - Assignee user ID
  - title - Title of MR
  - description - Description of MR
  - state_event - New state (close|reopen|merge)
  - labels (optional) - Labels for MR as a comma-separated list
  """
  def modify(id, mr_id, client, params \\ []), do: Tanuki.put("projects/#{id}/merge_request/#{mr_id}", client, params)

  @doc """
  PUT /projects/:id/merge_request/:merge_request_id/merge

  Merge changes submitted with MR using this API. If merge success you get 200 OK.

  If it has some conflicts and can not be merged - you get 405 and error message 'Branch cannot be merged'
  If merge request is already merged or closed - you get 405 and error message 'Method Not Allowed'
  If you don't have permissions to accept this merge request - you'll get a 401

  Parameters:
  - merge_commit_message (optional) - Custom merge commit message
  - should_remove_source_branch (optional) - if true removes the source branch
  - merged_when_build_succeeds (optional) - if true the MR is merge when the build succeeds
  """
  def accept(id, mr_id, client, params \\ []), do: Tanuki.put("projects/#{id}/merge_request/#{mr_id}/accept", client, params)
end
