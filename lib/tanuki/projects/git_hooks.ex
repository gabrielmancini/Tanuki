defmodule Tanuki.Projects.GitHooks do
  @moduledoc """
  Interact with git hooks (EE only)
  """

  @doc """
  GET /projects/:id/git_hooks

  Get a project git hook.
  """
  def list(id, client), do: Tanuki.get("projects/#{id}/git_hooks", client)

  @doc """
  POST /projects/:id/git_hooks

  Adds a git hook to a specified project.

  Parameters:
  - deny_delete_tag - Do not allow users to remove git tags with git push
  - commit_message_regex - Commit message regex
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/git_hooks", client, params)

  @doc """
  PUT /projects/:id/git_hooks

  Edits a git hook for a specified project.

  Parameters:
  - deny_delete_tag - Do not allow users to remove git tags with git push
  - commit_message_regex - Commit message regex
  """
  def modify(id, client, params), do: Tanuki.put("projects/#{id}/git_hooks", client, params)

  @doc """
  DELETE /projects/:id/git_hooks

  Removes a git hook from a project. This is an idempotent method and can be called multiple times. Either the git hook is available or not.
  """
  def delete(id, client), do: Tanuki.delete("projects/#{id}/git_hooks", client)
end
