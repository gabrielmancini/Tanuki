defmodule Tanuki.Projects.Issues do
  @doc """
  GET /projects/:id/issues

  Get a list of a project's issues.
  - iid (optional) Return the issue having the given iid
  - state (optional) Return all issues or just those that are opened or closed
  - labels (optional) Comma-separated list of label names
  - milestone (optional) The milestone title
  - order_by (optional) Return requests ordered by created_at or updated_at fields. Default is created_at
  - sort (optional) Return requests sorted in asc or desc order. Default is desc
  """
  def list(id, client, params \\ []), do: Tanuki.get("projects/#{id}/issues", client, params)

  @doc """
  GET /projects/:id/merge_request/:issue_id

  Get a single project issue.
  """
  def find(id, issue_id, client), do: Tanuki.get("projects/#{id}/issues/#{issue_id}", client)

  @doc """
  POST /projects/:id/issues

  Creates a new issue.

  Parameters:
  - title(required) - The title of an issue
  - description (optional) - The description of an issue
  - assignee_id (optional) - The ID of a user to assign issue
  - milestone_id (optional) - The ID of a milestone to assign issue
  - labels (optional) - Comma-separated label names for an issue
  """
  def create(id, client, params), do: Tanuki.post("projects/#{id}/issues", client, params)

  @doc """
  PUT /projects/:id/issues/:issue_id

  Updates an existing project issue. This call is also used to mark an issue as closed.

  If the operation is successful, a code of 200 and the updated issue is returned. If an error occurs, an error number and a message explaining the reason is returned.

  Parameters:
  - title (optional) - The title of an issue
  - description (optional) - The description of an issue
  - assignee_id (optional) - The ID of a user to assign the issue to
  - milestone_id (optional) - The ID of a milestone to assign the issue to
  - labels (optional) - Comma-separated label names for an issue
  - state_event (optional) - The state event of an issue. Set close to close the issue and reopen to reopen it
  """
  def modify(id, issue_id, client, params \\ []), do: Tanuki.put("projects/#{id}/issues/#{issue_id}", client, params)

end
