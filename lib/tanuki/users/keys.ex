defmodule Tanuki.Users.Keys do
  @doc """
  GET /user/keys

  Get your own keys
  """
  def mine(client), do: Tanuki.get("user/keys", client)

  @doc """
  GET /user/key/:id

  Gets your key by id
  """
  def find(id, client), do: Tanuki.get("user/keys/#{id}", client)

  @doc """
  GET /users/:uid/keys

  Gets the keys for the user with the passed uid
  """
  def find_for_user(uid, client), do: Tanuki.get("users/#{uid}/keys", client)

  @doc """
  POST /user/keys

  Required params:
  - title
  - key

  Adds a new key to your account
  """
  def create(client, params), do: Tanuki.post("user/keys", client, params)

  @doc """
  POST /users/:id/keys

  Required params:
  - title
  - key

  Adds a new key to the account with the id passed
  """
  def create_for_user(user_id, client, params), do: Tanuki.post("users/#{user_id}/keys", client, params)

  @doc """
  DELETE /user/keys/:id

  Remove your own key my id
  """
  def delete(id, client), do: Tanuki.delete("user/keys/#{id}", client)

  @doc """
  DELETE /users/:uid/keys/:id

  Remove your own key my id
  """
  def delete_for_user(uid, key_id, client), do: Tanuki.delete("users/#{uid}/keys/#{key_id}", client)

  @doc """
  GET /users/:uid/keys

  Where :uid is the id of the user
  """
  def list_for_user(uid, client), do: Tanuki.get("user/#{uid}/keys", client)
end
