defmodule Tanuki.NameSpaces do
  @doc """
  GET /namespaces

  Get a list of namespaces. (As user: my namespaces, as admin: all namespaces)
  """
  def list(client), do: Tanuki.get("namespaces", client)
end
