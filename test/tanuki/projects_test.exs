defmodule Tanuki.ProjectsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Projects

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Projects.list/2" do
    use_cassette "projects_list" do
      assert Enum.count(Projects.list(@client)) == 8
    end
  end

  test "Projects.find/2" do
    use_cassette "project_find" do
      assert Projects.find(7, @client).id == 7
    end
  end

  test "Projects.search/3" do
    use_cassette "project_search" do
      assert Projects.search("slfdkjslf", @client) |> Enum.empty?
    end
  end

  test "Projects.owned/2" do
    use_cassette "project_owned" do
      assert (Projects.owned(@client) |> Enum.count) == 8
    end
  end

  test "Projects.starred/2" do
    use_cassette "project_starred" do
      assert (Projects.starred(@client) |> Enum.count) == 2
    end
  end

  test "Projects.list_all/2" do
    use_cassette "projects_list" do
      assert Enum.count(Projects.list(@client)) == 8
    end
  end

  test "Projects.delete/2" do
    use_cassette "projects_delete" do
      assert Projects.delete(4, @client)
    end
  end

  test "Projects.fork/2" do
    use_cassette "projects_fork" do
      assert Projects.fork(7, @client)
    end
  end
end
