defmodule Tanuki.Project.MembersTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:8080/api/v3/")
  alias Tanuki.Projects.Members

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Members.list/1" do
    use_cassette "project_members_list" do
      assert (Members.list(1, @client) |> Enum.count) == 4
    end
  end

  test "Members.create/3" do
    params = %{ access_level: 10, user_id: 3 }
    use_cassette "project_members_create" do
      assert Members.create(1, @client, params)
    end
  end

  test "Members.modify/4" do
    params = %{
      access_level: 20
    }
    use_cassette "project_members_modify" do
      assert Members.modify(1, 3, @client, params)
    end
  end

  test "Members.delete/2" do
    use_cassette "project_members_delete", do: assert Members.delete(1, 3, @client)
  end
end
