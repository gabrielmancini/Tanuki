defmodule Tanuki.Project.Snippets do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "syWHotWuxcjy1tb5ounR"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Projects.Snippets

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Snippets.list/2" do
    use_cassette "project_snippets_list" do
      assert (Snippets.list(2, @client) |> Enum.count) == 1
    end
  end

  test "Snippets.find/3" do
    use_cassette "project_snippets_find" do
      assert Snippets.find(2, 50, @client).title == "Test Snippet"
    end
  end

  test "Snippets.create/3" do
    params = %{
      title: "New snippet",
      file_name: "snippet.exs",
      code: "code |> code",
      visibility_level: 20
    }

    use_cassette "projects_snippets_create" do
      assert Snippets.create(2, @client, params).id == 51
    end
  end

  test "Snippets.modify/3" do
    params = %{ visibility_level: 10 }

    use_cassette "project_snippets_modify" do
      assert Snippets.modify(2, 51, @client, params).id == 51
    end
  end

  test "Snippets.delete" do
    use_cassette "projects_snippets_delete" do
      assert Snippets.delete(2, 51, @client)
    end
  end
end
