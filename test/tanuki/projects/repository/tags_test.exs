defmodule Tanuki.Project.TagsTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "A28e6ZeJHNJQLN6r6yTv"}, "http://localhost:8080/api/v3/")
  alias Tanuki.Projects.Repository.Tags

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Tags.list/2" do
    use_cassette "project_tags_list", do: assert Tags.list(9, @client) |> Enum.count == 1
  end

end
