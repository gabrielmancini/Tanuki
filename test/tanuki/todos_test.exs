defmodule Tanuki.TodosTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "xHZdbgy4ukhM3e3s6RvZ"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Todos

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Todos.todos/1" do
    use_cassette "todos_list" do
      assert Todos.list(@client) |> Enum.count == 2
    end
  end

  test "Todos.done/2" do
    use_cassette "todos_done" do
      assert Todos.done(123, @client).state == "done"
    end
  end

  test "Todos.all_done/1" do
    use_cassette "todos_all_done" do
      assert Todos.all_done(@client) == 2
    end
  end
end
