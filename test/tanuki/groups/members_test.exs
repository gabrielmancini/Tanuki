defmodule Tanuki.Groups.MembersTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "A28e6ZeJHNJQLN6r6yTv"}, "http://localhost:8080/api/v3/")
  alias Tanuki.Groups.Members

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Members.list/1" do
    use_cassette "groups_members_list" do
      refute Members.list(2, @client) |> Enum.empty?
    end
  end

  test "Members.create/3" do
    member = %{
      user_id: 8,
      access_level: 20
    }
    use_cassette "groups_members_create" do
      assert Members.create(2, @client, member)
    end
  end

  test "Member.modify/4" do
    member = %{ access_level: 10}
    use_cassette "groups_members_modify" do
      assert Members.modify(2, 8, @client, member)
    end
  end

  test "Members.delete/3" do
    use_cassette "groups_members_delete" do
      assert Members.delete(2, 8, @client)
    end
  end
end
