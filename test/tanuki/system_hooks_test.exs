defmodule Tanuki.SystemHooksTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "g8zWsDJqs5LCn9-JRmJh"}, "http://localhost:3000/api/v3/")
  alias Tanuki.SystemHooks

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "SystemHooks.list/1" do
    use_cassette "system_hooks_list" do
      assert SystemHooks.list(@client) == []
    end
  end

  test "SystemHooks.create/3" do
    hook = %{url: "http://hook.domain.tld"}
    use_cassette "system_hooks_create" do
      assert SystemHooks.create(@client, hook).id == 2
    end
  end

  test "SystemHooks.delete/2" do
    use_cassette "system_hooks_delete" do
      assert SystemHooks.delete(1, @client)
    end
  end
end
