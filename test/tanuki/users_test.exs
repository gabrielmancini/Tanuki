defmodule Tanuki.UsersTest do
  use ExUnit.Case, async: false
  use ExVCR.Mock, adapter: ExVCR.Adapter.Hackney

  @client Tanuki.Client.new(%{private_token: "A28e6ZeJHNJQLN6r6yTv"}, "http://localhost:3000/api/v3/")
  alias Tanuki.Users

  setup_all do
    ExVCR.Config.cassette_library_dir("test/fixtures/vcr_cassettes")
    HTTPoison.start
  end

  test "Users.me/1" do
    use_cassette "user_me" do
      assert Users.me(@client).id == 1
    end
  end

  test "Users.issues/2" do
    use_cassette "user_issues" do
      assert Users.issues(@client) |> Enum.count == 13
    end
  end

  test "Users.list/2" do
    use_cassette "users_list2" do
      assert Enum.count(Users.list(@client)) == 20
    end
  end

  test "Users.find/2" do
    use_cassette "users_find2" do
      assert Users.find(2, @client).id == 2
    end
  end

  test "Users.create/2" do
    new_user = %{
      email: "myemail@gitlab.tld",
      password: "tanuki!!!!",
      username: "Tanuki",
      name:     "Mepmep",
      confirm:  false       # optional
    }

    use_cassette "users_create" do
      assert Users.create(@client, new_user).id > 20
    end
  end

  test "Users.delete/2" do
    use_cassette "users_delete" do
      assert Users.delete(15, @client).id == 15
    end
  end

  test "Users.block/2" do
    use_cassette "users_block" do
      assert Users.block(7, @client)
    end
  end

  test "Users.unblock/2" do
    use_cassette "users_unblock" do
      assert Users.unblock(7, @client)
    end
  end
end
